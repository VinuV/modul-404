import java.util.*;

public class Calculator {
	
	public static void add(int x, int y) {
		System.out.println(x + y);
	}
	
	public static void sub(int x, int y) {
		 System.out.println(x - y);
	}

	public static void mul(int x, int y) {
		 System.out.println(x * y);
	}

	public static void div(int x, int y) {
		try {
		  System.out.println(x/y);
		} catch(ArithmeticException e) {
			System.out.println("Division mit 0 geht nicht!!!");	
		}
	}


	public static void main(String[] args) { 
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie zwei Zahlen ein:");
		int x = scanner.nextInt();
		int y = scanner.nextInt();
		
		System.out.println("Welche Rechenoperation? Gebe das Symbol ein.");
		System.out.println("+, -, *, /");
		
		
		String op = scanner.next();
		
		if(op.equals("+")) {
			add(x, y);
		}
		
		if(op.equals("-")) {
			sub(x, y);
		}

		if(op.equals("*")) {
			mul(x, y);
		}

		if(op.equals("/")) {
			div(x, y);
		}
		

		
	
		 
  } 
}
